const validetor = require('./Helper/validation')
const default_record_path = '/var/lib/asterisk/sounds/blc/user_rec/'
const weTalk = require('./Helper/WeTalk/BilrunHttpMethods')


function setRecordFile(record) {
    record = record.replace('.mp3','.sln');
    record = record.replace('.wav','.sln');
    return default_record_path + record;
}

module.exports = async  function (call) {

    try {
        let sayDataCallerID = [
            {Recording:setRecordFile("114750_30012020141352.mp3")},
            {Digits:call.CallerID}
        ]
    
        let sayDataDTMF = [
            {Recording:setRecordFile("114750_30012020141352.mp3")},
            {Digits:call.DTMF}
        ]
        let sayDataCallerIDHebrew = [
            {Recording:setRecordFile("115822_27022020154444.mp3")},
            {Digits:call.CallerID}
        ]
        console.log(setRecordFile("115822_27022020154444.mp3"));
    
        let sayDataDTMFHebrew = [
            {Recording:setRecordFile("115822_27022020154444.mp3")},
            {Digits:call.DTMF}
        ]
    
        let sayDataCallerIDEnglish = [
            {Recording:setRecordFile("115822_27022020111027.mp3")},
            {Digits:call.CallerID}
        ]
    
        let sayDataDTMFEnglish = [
            {Recording:setRecordFile("115822_27022020111027.mp3")},
            {Digits:call.DTMF}
        ]
    
        console.log('the LayerID is ' + call.LayerID);
        let respondToChargeResult,currentCallerbalance;
    
        switch (call.LayerID) {
            // layer "0" - Welcome
            case '0':
                //We create a session in Billrun using Login method.
                let resultsInJsonFormat = await weTalk.login().catch(err => {
                        console.error(err);
                    })
                    console.log("resultsInJsonFormat: " + resultsInJsonFormat);
                    
                let loginSessionResults = JSON.parse(resultsInJsonFormat);
                console.log("loginSessionResults: " + loginSessionResults);

                //We check that we got a valid login session
                if (loginSessionResults && loginSessionResults.hasOwnProperty('id')) {
                    //We set the session id we got from Billrun
                    call.SetParam("WeTalkCallSessionId",results.id);
                    console.log('WeTalkCallSessionId is ' + call.GetParam('WeTalkCallSessionId'));
    
                //We direct the call to pick language layer - "1"
                call.GoToLayer(1);  
                }
                //If we did not got a valid login session, we need to decide what to to with the client
                else {
                    console.log("Not valid login session");
                    //for debug
                    call.SetParam("WeTalkCallSessionId","debugVoicenter");
                    console.log('WeTalkCallSessionId is :' + call.GetParam('WeTalkCallSessionId'));
                    //for debug we conitnue to direct the call to language selection
                    call.GoToLayer(1);  
                }
    
            break;
    
    
            //Charge subscription - Hebrew
            case '30' :
                // We update the costume data with the Caller ID
                call.SetParam("WeTalkNumber",call.CallerID);
                console.log('WeTalkNumber is: ' + call.GetParam('WeTalkNumber'));
                //We direct the call to enter the secret code
                call.GoToLayer(37);  
    
            break;
    
    
            //Charge subscription - English
            case '33' :
                // We update the costume data with the Caller ID
                call.SetParam("WeTalkNumber",call.CallerID);
                console.log('WeTalkNumber ' + call.GetParam('WeTalkNumber'));
                //We direct the call to enter the secret code
                call.GoToLayer(38); 
            
            break;
    
    
            //Enter secret code to charge subscription - Hebrew
            case '37' :
                console.log('the DTMF(secret code that was entered) is ' + call.DTMF);
                console.log('WeTalkCallSessionId is' + call.GetParam('WeTalkCallSessionId'));
                console.log('WeTalkNumber is' + call.GetParam('WeTalkNumber'));
    
                //We send to Billrun a request to charge the subscription using topup_prepaid_balance method
                respondToChargeResult = await weTalk.topup_prepaid_balance(call.GetParam('WeTalkCallSessionId'),call.GetParam('WeTalkNumber'),call.DTMF).catch(err => {
                    console.error(err)
                })
                console.log("respondToChargeResult :" + respondToChargeResult);
    
                //We check if the charge was successful
                if (respondToChargeResult && respondToChargeResult.hasOwnProperty('reason_code')) {

                    if (respondToChargeResult.reason_code == 1000 ) {
    
                        //We say to the caller his\current balance
                        let sayArray = [];
                        let ids = require('./Helper/WeTalk/pp_includes_external_id.json')
                
                        respondToChargeResult.data.foreach( el => {
                    
                        value.push({Recording:setRecordFile(ids['HE'][el.pp_includes_external_id])})
                        value.push({Digits:el.value});
    
                        })
                        console.log("!!! ",sayArray)
                        //where do we need to direct the call after? 26 Menu - hebrew.
                        call.GoToLayer(26);
                    
                        }
                    // If the charge request was unsuccessful, we direct the caller to the unsuccessful charge layer    
                    else {
                    call.GoToLayer(41);
                    }
                }    
                // If the all charge request was unsuccessful, we direct the caller to the unsuccessful charge layer    
                else {
                    call.GoToLayer(41);
                    }
            
            break;
    
           //Enter secret code to charge subscription - English
           case '38' :
            console.log('the DTMF(secret code that was entered) is ' + call.DTMF);
            console.log('WeTalkCallSessionId is' + call.GetParam('WeTalkCallSessionId'));
            console.log('WeTalkNumber is' + call.GetParam('WeTalkNumber'));
    
    
            //We send to Billrun a request to charge the subscription using topup_prepaid_balance method
            respondToChargeResult = await weTalk.topup_prepaid_balance(call.GetParam('WeTalkCallSessionId'),call.GetParam('WeTalkNumber'),call.DTMF).catch(err => {
                console.error(err)
            })
            console.log("respondToChargeResult" + respondToChargeResult);
    
            //We check if the charge was successful
            if (respondToChargeResult && respondToChargeResult.hasOwnProperty('reason_code')) {

                if (respondToChargeResult.reason_code == 1000 ) {

                    //We say to the caller his\current balance
                    let sayArray = [];
                    let ids = require('./Helper/WeTalk/pp_includes_external_id.json')
            
                    respondToChargeResult.data.foreach( el => {
                
                    value.push({Recording:setRecordFile(ids['EN'][el.pp_includes_external_id])})
                    value.push({Digits:el.value});

                    })
                    console.log("!!! ",sayArray)
                    //where do we need to direct the call after? 26 Menu - hebrew.
                    call.GoToLayer(27);
                
                    }
                // If the charge request was unsuccessful, we direct the caller to the unsuccessful charge layer    
                else {
                call.GoToLayer(42);
                }
            }    
            // If the all charge request was unsuccessful, we direct the caller to the unsuccessful charge layer    
            else {
                call.GoToLayer(42);
                }
        
            break;
    
    
            //Subscription balance - Hebrew
            case '31' :
    
                //We send to Billrun a request to get the current user balance using get_prepaid_balances method
                currentCallerbalance = await weTalk.get_prepaid_balances(call.GetParam('WeTalkCallSessionId'), call.CallerID).catch(err => {
                    console.error(err)
                })
                console.log("currentCallerbalance" + currentCallerbalance);
    
                //We check if the balance request was successful
                if (currentCallerbalance && currentCallerbalance.hasOwnProperty('reason_code')) {

                    if (currentCallerbalance.reason_code == 1000 ) {
    
                        let sayArray = [];
                        let ids = require('./Helper/WeTalk/pp_includes_external_id.json')
                
                        currentCallerbalance.data.foreach( el => {
                    
                            value.push({Recording:setRecordFile(ids['HE'][el.pp_includes_external_id])})
                            value.push({Digits:el.value});
    
                     })
                        console.log("!!! ",sayArray)
    
                        //We say to the caller his\her current balance and we direct the call to the actions menu.
                        call.Say({sayData: el },26,"HE")
                    }
    
                    //If the balance request was un-successful, we direct the caller to the unsuccessful balance request layer.
                    else {
                        call.GoToLayer(43);
                    }
                }
                //If the All balance request was un-successful, we direct the caller to the unsuccessful balance request layer.
                else {
                    call.GoToLayer(43);
                }
    
            break;
    
    
            //Subscription balance - English
            case '34' :
    
                //We send to Billrun a request to get the current user balance using get_prepaid_balances method
                currentCallerbalance = await weTalk.get_prepaid_balances(call.GetParam('WeTalkCallSessionId'), call.CallerID).catch(err => {
                    console.error(err)
                })
                console.log("currentCallerbalance" + currentCallerbalance.data);
    
                //We check if the balance request was successful
                if (currentCallerbalance && currentCallerbalance.hasOwnProperty('reason_code')) {

                    if (currentCallerbalance.reason_code == 1000 ) {
    
                        let sayArray = [];
                        let ids = require('./Helper/WeTalk/pp_includes_external_id.json')
                
                        currentCallerbalance.data.foreach( el => {
                    
                            value.push({Recording:setRecordFile(ids['EN'][el.pp_includes_external_id])})
                            value.push({Digits:el.value});
    
                     })
                        console.log("!!! ",sayArray)
    
                        //We say to the caller his\her current balance and we direct the call to the actions menu.
                        call.Say({sayData: el },27,"EN")
                    }
    
                    //If the balance request was un-successful, we direct the caller to the unsuccessful balance request layer.
                    else {
                        call.GoToLayer(44);
                    }
                }
                //If the All balance request was un-successful, we direct the caller to the unsuccessful balance request layer.
                else {
                    call.GoToLayer(44);
                }
    
            break;

    
            //Charge subscription - new entered subscription - Hebrew
            case '32' :
                // Please enter a mobile number - Hebrew
                call.GoToLayer(39)  
    
            break;
    
    
            //Charge subscription - new entered subscription - English
            case '35' :
                // Please enter a mobile number - English
                call.GoToLayer(40)  
            
            break;
    
    
            // Please enter a mobile number - Hebrew
            case '39' :
                // We update the costume data with the new subscription - DTMF
                call.SetParam("WeTalkNumber",call.DTMF);
                console.log('WeTalkNumber ' + call.GetParam('WeTalkNumber'));
                //We direct the call to enter the secret code
                call.GoToLayer(37);  
            
            break;
    
            // Please enter a mobile number - English
            case '40' :
                // We update the costume data with the new subscription - DTMF
                call.SetParam("WeTalkNumber",call.DTMF);
                console.log('WeTalkNumber ' + call.GetParam('WeTalkNumber'));
                //We direct the call to enter the secret code
                call.GoToLayer(38);  
            
            break;
    



            // if we got a request from a layer that we did not configure logic for it  we will direct the call to the main layer - o  
            default:
                call.GoToLayer(0)
            break;



    }

    
    
    } catch(e) {
        console.error(e);
    }
}
