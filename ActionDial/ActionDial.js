const express = require('express');
const app = express();
const port = 8080;

const responseObj = {
  "ACTION": "DIAL",
  "CALLER_ID": "0722776772",
  "CALLER_NAME": "",
  "MAX_CALL_DURATION": 7200,
  "MAX_DIAL_DURATION": 30,
  "NEXT_VO_ID": 1,
  "RECORDING": "yes",
  "STATUS": 0,
  "TARGETS": [
    {
      "TARGET": "0523574321",
      "TYPE": "PHONE"
    }
  ]
}
app.get('/Ivr/ActioDial', (req, res) => res.send(responseObj))

app.listen(port, () => console.log(`Example app listening at http://185.138.169.160:${port}/`))
