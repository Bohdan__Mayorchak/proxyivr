const iStore_request = require('./Helper/iStore216/iStoreMethod').iStore_request;

module.exports = async  function (call) {

    try {

        console.log('the LayerID is ' + call.LayerID);

        switch (call.LayerID) {
            // layer that we send requests to iStore
            case '100':
                //We set queue param - the call was not sent from one of the known layers than specified for a queue
                call.SetParam("sendRequestFromQueue","NoQueue");
                console.log('sendRequestFromQueue is :' + call.GetParam('sendRequestFromQueue'));
                //We send the caller callerId and the queue name that the call was directed from
                iStore_request(call.CallerID,call.GetParam('sendRequestFromQueue'));
                //End the call
                call.GoToLayer(101);
                break;

            // layer that we send requests to iStore - from PRIVATE queue
            case '108':
                //We set queue param
                call.SetParam("sendRequestFromQueue","PRIVATE");
                console.log('sendRequestFromQueue is :' + call.GetParam('sendRequestFromQueue'));
                //We send the caller callerId and the queue name that the call was directed from
                iStore_request(call.CallerID,call.GetParam('sendRequestFromQueue'));
                //End the call
                call.GoToLayer(101);
                break;

            // layer that we send requests to iStore - from BUSINESS queue
            case '109':
                //We set queue param
                call.SetParam("sendRequestFromQueue","BUSINESS");
                console.log('sendRequestFromQueue is :' + call.GetParam('sendRequestFromQueue'));
                //We send the caller callerId and the queue name that the call was directed from
                iStore_request(call.CallerID,call.GetParam('sendRequestFromQueue'));
                //End the call
                call.GoToLayer(101);
                break;

            // layer that we send requests to iStore - from SERVICE queue
            case '110':
                //We set queue param
                call.SetParam("sendRequestFromQueue","SERVICE");
                console.log('sendRequestFromQueue is :' + call.GetParam('sendRequestFromQueue'));
                //We send the caller callerId and the queue name that the call was directed from
                iStore_request(call.CallerID,call.GetParam('sendRequestFromQueue'));
                //End the call
                call.GoToLayer(101);
                break;

            // layer that we send requests to iStore - from SUPPORT queue
            case '111':
                //We set queue param
                call.SetParam("sendRequestFromQueue","SUPPORT");
                console.log('sendRequestFromQueue is :' + call.GetParam('sendRequestFromQueue'));
                //We send the caller callerId and the queue name that the call was directed from
                iStore_request(call.CallerID,call.GetParam('sendRequestFromQueue'));
                //End the call
                call.GoToLayer(101);
                break;

            // layer that we send requests to iStore - from ENGLISH queue
            case '112':
                //We set queue param
                call.SetParam("sendRequestFromQueue","ENGLISH");
                console.log('sendRequestFromQueue is :' + call.GetParam('sendRequestFromQueue'));
                //We send the caller callerId and the queue name that the call was directed from
                iStore_request(call.CallerID,call.GetParam('sendRequestFromQueue'));
                //End the call
                call.GoToLayer(101);
                break;


            // if we got a request from a layer that we did not configure logic for it, we make the same logic.
            default:
                //We set queue param - the call was not sent from one of the known layers than specified for a queue
                call.SetParam("sendRequestFromQueue","NoQueue");
                console.log('sendRequestFromQueue is :' + call.GetParam('sendRequestFromQueue'));
                //We send the caller callerId and the queue name that the call was directed from
                iStore_request(call.CallerID,call.GetParam('sendRequestFromQueue'));
                //End the call
                call.GoToLayer(101);
                break;


        }

    } catch(e) {
        console.error(e);
    }
}
