const validetor = require('./Helper/validation')
const default_record_path = '/var/lib/asterisk/sounds/blc/user_rec/'
const callback_request = require('./Helper/SmartDos8266/SmartDosAddCallToCampign.js');


function setRecordFile(record) {
    record = record.replace('.mp3','.sln');
    record = record.replace('.wav','.sln');
    return default_record_path + record;
}

module.exports = async  function (call) {

    let sayDataCallerID = [
        {Recording: setRecordFile("114791_17052020161526.mp3")},
        {Digits: call.CallerID}
    ]

    let sayDataDTMF = [
        {Recording: setRecordFile("114791_17052020161526.mp3")},
        {Digits: call.GetParam('TempCallBackNumber')}
    ]

    console.log('the LayerID is ' + call.LayerID);

    switch (call.LayerID) {


        //We direct the calls by their origin to Callback service
        // Case 21 - Service call
        case '21':
            //We set call type to service
            call.SetParam("CallType", "Service");
            console.log('The Call Type  is : ' + call.GetParam('CallType'));
            call.SetParam("TempCallBackNumber", call.CallerID);
            console.log('The TempCallBackNumber number is : ' + call.GetParam('TempCallBackNumber'));

            //Then we say to the client the number we identified. and direct the call to Callback
            call.Say({sayData: sayDataCallerID}, 23, "HE")
            //call.GoToLayer(23);

            break;

        // Case 27 - Sales call
        case '27':
            //We set call type to service
            call.SetParam("CallType", "Sales");
            console.log('The Call Type  is : ' + call.GetParam('CallType'));
            call.SetParam("TempCallBackNumber", call.CallerID);
            console.log('The TempCallBackNumber number is : ' + call.GetParam('TempCallBackNumber'));

            //Then we say to the client the number we identified. and direct the call to Callback
            call.Say({sayData: sayDataCallerID}, 23, "HE")
            break;

        // Case 28 - Finance call
        case '28':
            //We set call type to service
            call.SetParam("CallType", "Finance");
            console.log('The Call Type  is : ' + call.GetParam('CallType'));
            call.SetParam("TempCallBackNumber", call.CallerID);
            console.log('The TempCallBackNumber number is : ' + call.GetParam('TempCallBackNumber'));

            //Then we say to the client the number we identified. and direct the call to Callback
            call.Say({sayData: sayDataCallerID}, 23, "HE")
            break;


        //Callback - to confirm the caller id press 1. to change please press 2
        case '23':
            console.log('the DTMF is ' + call.DTMF);
            switch (call.DTMF) {
                // if dilaed "1" the caller approve the caller id we identified
                case "1":
                    console.log('The Call Type  is : ' + call.GetParam('CallType'));

                    //We set the caller caller id as the callback number
                    call.SetParam("CallBackNumber", call.GetParam('TempCallBackNumber'));
                    console.log('The CallBack number is : ' + call.GetParam('CallBackNumber'));

                    //Then we need to update the rellevent callback service
                    switch (call.GetParam('CallType')) {
                        case 'Service':
                            callback_request.ServiceCallbak(call.GetParam('CallBackNumber'));
                            break;

                        case 'Sales':
                            callback_request.SalesCallbak(call.GetParam('CallBackNumber'));
                            break;

                        case 'Finance':
                            callback_request.FinanceCallbak(call.GetParam('CallBackNumber'));
                            break;
                    }

                    //Now we will forward the call to the external service layer that send the request to the client CRM
                    call.GoToLayer(24);

                    break;

                // if dialed "2" the client wants to enter a new caller id
                case "2":
                    //"please enter a new phone number"
                    call.GoToLayer(26)
                    break;

                // if the caller did not choose an options or pressed on a wrong digit
                default:
                    //"you dialed the wrong option" directs afterwards to the main layer - '13'
                    call.GoToLayer(23)
                    break;
            }

            break;

        //Please enter a new number
        case '26':
            //We say to the client the number we identified
            call.SetParam("TempCallBackNumber", call.DTMF);
            console.log('The TempCallBackNumber number is : ' + call.GetParam('TempCallBackNumber'));
            call.GoToLayer(30)

            //Then we say to the client the number we identified. and direct the call to Callback
            //call.Say({sayData: sayDataDTMF}, 23, "HE")

            break;

        case '30':

            //Then we say to the client the number we identified. and direct the call to Callback
            call.Say({sayData: sayDataDTMF}, 23, "HE")

            break;

        //Callback - Finance choose if to direct the call to callback or not
        case '34':
            console.log('the DTMF is ' + call.DTMF);
            switch (call.DTMF) {
                // if dilaed "1" the caller wants to be directed to callback finance
                case "1":

                    call.GoToLayer(28);

                    break;

                // if the caller did not choose an options or pressed other digits we direct the call to finance extension
                default:
                    call.GoToLayer(6)
                    break;
            }
            break;



    }
}


