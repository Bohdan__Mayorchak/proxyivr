const fetch = require('node-fetch');

module.exports = {
    http: async (url, method ,body,headers,timeout = 5000) => {
        return fetch(url, {
            method: method,
            body:    body,
            headers: headers,
            timeout
        }).then(res => res.json())
        .then(json => console.log(json));
    }
}