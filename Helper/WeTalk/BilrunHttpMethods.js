const { URLSearchParams } = require('url');
 
let login = () => {
    const body = new URLSearchParams();
body.append('rest_data', `{
    "user_auth": {
        "user_name": "guy.s",
        "password": "70a7978714e77b36cea62d13e9bad470",
        "version": "1"
    },
    "application_name": "RestTest"
}`);
body.append('method','login');
body.append('input_type','JSON');
body.append('response_type','JSON');
body.append('XDEBUG_SESSION_START','netbeans-xdebug')


const httpReqeust = require('../http');
let url = 'http://10.21.2.10/custom/service/v4_1_custom/rest.php'
let headers = { 
    "Content-Type" : "application/x-www-form-urlencoded"
}

 return httpReqeust.http(url,'post',body,headers).then( results => {
    console.log(results)
    return results
})
}


let get_prepaid_balances = (sessionId, phoneNumber) => {
    const body = new URLSearchParams();
body.append('rest_data', `{
    "session": ${sessionId} ,
    "action": "get_prepaid_balances",
    "data": {
        "phone_number": ${phoneNumber},
        "semi_authenticate": true
    }
}`);
body.append('method','selfcare');
body.append('input_type','JSON');
body.append('response_type','JSON');
body.append('XDEBUG_SESSION_START','netbeans-xdebug')


const httpReqeust = require('../http');
let url = 'http://10.21.2.10/custom/service/v4_1_custom/rest.php'
let headers = { 
    "Content-Type" : "application/x-www-form-urlencoded"
}

 return httpReqeust.http(url,'post',body,headers).then( results => {
    console.log(results)
    return results
})
}


let topup_prepaid_balance = (sessionId, phoneNumber, secret) => {
    const body = new URLSearchParams();
body.append('rest_data', `{
    "session": ${sessionId},
    "action": "topup_prepaid_balance",
    "data": {
        "phone_number": ${phoneNumber},
        "secret": ${secret},
        "semi_authenticate": true
    },
    "application_name": "RestTest"
}`);
body.append('method','selfcare');
body.append('input_type','JSON');
body.append('response_type','JSON');
body.append('XDEBUG_SESSION_START','netbeans-xdebug')


const httpReqeust = require('../http');
let url = 'http://10.21.2.10/custom/service/v4_1_custom/rest.php'
let headers = { 
    "Content-Type" : "application/x-www-form-urlencoded"
}

 return httpReqeust.http(url,'post',body,headers).then( results => {
    console.log(results)
    return results
})
}

module.exports = {
    get_prepaid_balances,
    login,
    topup_prepaid_balance
}