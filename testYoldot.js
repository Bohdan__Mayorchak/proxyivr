const validetor = require('./Helper/validation')
const default_record_path = '/var/lib/asterisk/sounds/blc/user_rec/'

function setRecordFile(record) {
    record = record.replace('.mp3','.sln');
    record = record.replace('.wav','.sln');
    return default_record_path + record;
}

function subStringId(callerID) {
    const id = callerID;
    let IdForCRM;
    if (id.startsWith('0')) {
    IdForCRM = id.slice(1);
        if (IdForCRM.startsWith('0')) {
            IdForCRM = IdForCRM.slice(1);
        }
    }
    else 
        IdForCRM = id;
    // console.log(IdForCRM);
    return IdForCRM;
  }

module.exports = async  function (call) {

    let sayDataForLayer0 = [
        {Recording:setRecordFile("104672_27032020185249.mp3")}
    ]

    let sayDataForLayer58 = [
        {Recording:setRecordFile("104672_18112019142335.mp3")},
        {Digits:call.DTMF}
    ]

    // let sayDataForLayer65 = [
    //     {Recording:setRecordFile("104672_18112019142349.mp3")},
    //     {Digits:call.GetParam('id')}
    // ]
    
    // let sayDataForLayer70 = [
    //     {Recording:setRecordFile("104672_18112019142404.mp3")},
    //     {Digits:call.GetParam('id')}
    // ]



    console.log('the LayerID is ' + call.LayerID);

    switch (call.LayerID) {
        //'0' is the main layer.
        case '0':            
            //call.Say({Recording:setRecordFile("104672_18112019142156.mp3")}, 1, "HE")
            call.Say({sayData: sayDataForLayer0},1,"HE")

        break;

        case '58':
            console.log('the DTMF is ' + call.DTMF);
            if(subStringId(call.DTMF).length == 9){
                console.log('58 cheking call.DTMF');
                call.SetParam('idNumber',subStringId(call.DTMF));
                call.SetParam('id',call.DTMF);
                call.Say({sayData: sayDataForLayer58},59,"HE")
            }
            else {
                call.GoToLayer(58)  
            }
        break;  

        case '59':
            console.log('59 the DTMF is ' + call.DTMF);
            switch (call.DTMF) {
                // if dilaed "1"  the caller approve the id we identified
                case "1":
                    
                    call.GoToLayer(61)
                break;
                
                // if dialed "2" the client doesn't approve the id we identified
                case "2":  
                    
                    call.GoToLayer(58)
                    break;
        
                // if the caller did not choose an options or pressed on a wrong digit     
                default:
                     //"you dialed the wrong option" directs afterwards to the main layer - '0'
                    call.GoToLayer(59)
                    break;
            } 

        break;


        case '65':
            console.log('65 id =',call.GetParam('id'));
            //call.Say({sayData: sayDataForLayer65},69,"HE")
            call.Say({sayData: [{Recording:setRecordFile("104672_18112019142349.mp3")},{Digits:call.GetParam('id')}]}, 65, "HE")    

        break;

       

        case '70':
            console.log('70 id =',call.GetParam('id'));
            //call.Say({sayData: sayDataForLayer70},0,"HE")
            call.Say({sayData: [{Recording:setRecordFile("104672_18112019142404.mp3")},{Digits:call.GetParam('id')}]}, 0, "HE")    

        break;
    
    }
}
