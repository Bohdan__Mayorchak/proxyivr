
const PAZ_request = require('./Helper/PAZHelper/requestToPAZ').PAZ_request;

module.exports = async  function (call) {

    console.log('the LayerID is ' + call.LayerID);

    switch (call.LayerID) {
        //'0' is the main layer.
        case '0':            
            console.log('the DTMF is ' + call.DTMF);

            const data = {
                "METHOD": "IVR_LAYER_INPUT",
                "DATA": {    
                    "DID": call.Did || '',
                    "CALLER_ID": call.CallerID || '',
                    "IVR_UNIQUE_ID": call.CallID || '',
                    "DTMF": call.DTMF || '',
                    "LAYER_ID": call.LayerID || '',
                    "PREVIOUS_LAYER_ID": call.PreviousLayerID || ''
                }
            }
            console.log('dataPAZ', JSON.stringify(data));
            let response = await PAZ_request(data);
            console.log('LayerNumber', Number(response.data.LAYER));
            call.GoToLayer(Number(response.data.LAYER));        
        break;
    
    }
}
