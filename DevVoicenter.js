const validetor = require('./Helper/validation')
const default_record_path = '/var/lib/asterisk/sounds/blc/user_rec/'

function setRecordFile(record) {
    record = record.replace('.mp3','.sln');
    record = record.replace('.wav','.sln');
    return default_record_path + record;
}

module.exports = async  function (call) {

    let sayDataCallerID = [
        {Recording:setRecordFile("71459_22042019133439.mp3")},
        {Digits:call.CallerID}
    ]

    let sayDataDTMF = [
        {Recording:setRecordFile("71459_30012020202951.mp3")},
        {Digits:call.DTMF}
    ]

    let sayDataDTMF2 = [
        {Recording:setRecordFile("71459_30012020202951.mp3")},
        {Digits:call.DTMF}
    ]

    let sayDataDIGITS = [
        {Recording:setRecordFile("71459_30012020202951.mp3")},
        {Digits:'0123456789'}
    ]

    let sayDataNUMBER = [
        {Recording:setRecordFile("71459_30012020202951.mp3")},
        {Number:'1234'}
    ]

    let sayDataDate = [
        {Recording:setRecordFile("71459_30012020202951.mp3")},
        {Date:'2017-12-25'}
    ]

    let sayDataAll = [
        {Recording:setRecordFile("71459_30012020202951.mp3")},
        {Date:'2017-12-25'},
        {Recording:setRecordFile("71459_30012020202951.mp3")},
        {Digits:'0123456789'},
        {Recording:setRecordFile("71459_30012020202951.mp3")},
        {Number:'1234'},
        {Recording:setRecordFile("71459_30012020202951.mp3")}
    ]

    let sayDataAll2 = [

    {Recording:setRecordFile("115822_27022020094804.mp3")},{Number:"2956"},
    {Recording:setRecordFile("115822_30032020174704.mp3")},{Number:"24"},
    {Recording:setRecordFile("115822_30032020174738.mp3")},{Number:"2999"},
    {Recording:setRecordFile("115822_30032020174958.mp3")},{Number:"70"},
    {Recording:setRecordFile("115822_30032020174837.mp3")},{Number:"0"},
    {Recording:setRecordFile("115822_30032020174906.mp3")},{Number:"40"},
    {Recording:setRecordFile("115822_30032020175043.mp3")},{Number:"2989"},
    {Recording:setRecordFile("115822_30032020174704.mp3")},{Number:"30"},
    {Recording:setRecordFile("115822_30032020174738.mp3")},{Number:"3000"},
    {Recording:setRecordFile("115822_30032020174958.mp3")},{Number:"50"},
    {Recording:setRecordFile("115822_30032020174837.mp3")},{Number:"0"},
    {Recording:setRecordFile("115822_30032020174906.mp3")},{Number:"30"},
    {Recording:setRecordFile("115822_30032020175043.mp3")}
    ]

    
    console.log('the LayerID is ' + call.LayerID);

    switch (call.LayerID) {
        // 0 - saydigits 0-9 english
        case '0':            
                call.Say({sayData: sayDataDIGITS},0,"EN")            
        break;

        // 1 - saydigits 0-9 hebrew
        case '1':            
                call.Say({sayData: sayDataDIGITS},1,"HE")            
        break;
        
        // 2 - say number 1234 english
        case '2':            
                call.Say({sayData: sayDataNUMBER},2,"EN")            
        break;
        
        // 3 - say number 1234 hebrew
        case '3':            
                call.Say({sayData: sayDataNUMBER},3,"HE")            
        break;

        // 5 - say date 14/03/2019 english
        case '5':            
                call.Say({sayData: sayDataDate},5,"EN")            
        break;
        
        // 6 - say date 14/03/2019 heberew
        case '6':            
                call.Say({sayData: sayDataDate},6,"HE")            
        break;        
                
        // 8 - say all data heberew
        case '8':            
                call.Say({sayData: sayDataAll2},8,"HE")            
        break;  

        // 9 - say all data heberew
        case '9':            
                call.Say({sayData: sayDataAll2},9,"EN")            
        break; 
        
        //'301' - Aproval of the caller id (1 - aprove, 2 - enter new caller id)
        case '301':
            console.log('the DTMF is ' + call.DTMF);
            switch (call.DTMF) {
                // if dilaed "1" the caller approve the caller id we identified
                case "1":
                    //we check that the current caller id is a valid mobile israel phone number    
                     if(validetor.IsisraelMobile(call.CallerID)){
                    // We update the costume data with the aprove phone number
                    call.SetParam("SendSMSTo",call.CallerID)
                    //Goodbye
                    call.GoToLayer(303) 
                }
                    // not a valid Israel mobile number. "please dialed a new number".
                    else {
                    call.GoToLayer(302)  
                }
 
                break;
                
                // if dialed "2" the client wants to enter a new caller id 
                case "2":  
                    //"please enter a new phone number"
                    console.log('301 2');
                    call.GoToLayer(304)
                    break;
        
                // if the caller did not choose an options or pressed on a wrong digit     
                default:
                     //"you dialed the wrong option" directs afterwards to the main layer - '13'
                    call.GoToLayer(301)
                    break;
            }             
                
        break;

        // layer '304' the caller enters a new mobile phone number
        case '304':
                // we check that the new caller id is a valid mobile israel phone number    
                if(validetor.IsisraelMobile(call.DTMF)){
                    // We update the costume data with the aprove phone number
                    call.SetParam("SendSMSTo",call.DTMF)
                    //Please aprove the mobile number
                    call.Say({sayData: sayDataDTMF2},306,"HE")
                }
                // not a valid Israel mobile number. "please dialed a new number".
                else {
                    call.GoToLayer(305)  
                }
        break;   

         // layer '302' the caller callerId is not an Israeli mobile number, please enter a valid number
         case '302':
                // we check that the new caller id is a valid mobile israel phone number    
                if(validetor.IsisraelMobile(call.DTMF)){
                    // We update the costume data with the aprove phone number
                    call.SetParam("SendSMSTo",call.DTMF)
                    //Please aprove the mobile number
                    call.Say({sayData: sayDataDTMF},306,"HE")
                }
                // not a valid Israel mobile number. "please dialed a new number".
                else {
                    call.GoToLayer(305)  
                }
        break;  

        //'306' - Aproval of the caller id (1 - aprove, 2 - enter new caller id) - we added 306 because using 301 twice update the current caller id and not the DTMF
        case '306':
            console.log('the DTMF is ' + call.DTMF);
            switch (call.DTMF) {
                // if dilaed "1" the caller approve the caller id we identified
                case "1":
                    //Goodbye
                    call.GoToLayer(303) 
                break;
                
                // if dialed "2" the client wants to enter a new caller id 
                case "2":  
                    //"please enter a new phone number"
                    console.log('301 2');
                    call.GoToLayer(304)
                    break;
        
                // if the caller did not choose an options or pressed on a wrong digit     
                default:
                     //"you dialed the wrong option" directs afterwards to the main layer - '13'
                    call.GoToLayer(306)
                    break;
            }             
                
        break;

        // layer '305' the dialed callerId is not an Israeli mobile number, please enter a valid number
        case '305':
            // we check that the new caller id is a valid mobile israel phone number    
            if(validetor.IsisraelMobile(call.DTMF)){
                // We update the costume data with the aprove phone number
                call.SetParam("SendSMSTo",call.DTMF)
                //Please aprove the mobile number
                call.Say({sayData: sayDataDTMF},306,"HE")
            }
            // not a valid Israel mobile number. "please dialed a new number".
            else {
                call.GoToLayer(305)  
            }
        break;  
    
        // 400s layers are for more details SMS
        //'400' - We say the caller ID that we identified
        case '400':            
                //"The caller id is:  "
                call.Say({sayData: sayDataCallerID},401,"HE")            
        break;

        //'401' - Aproval of the caller id (1 - aprove, 2 - enter new caller id)
        case '401':
            console.log('the DTMF is ' + call.DTMF);
            switch (call.DTMF) {
                // if dilaed "1" the caller approve the caller id we identified
                case "1":
                    //we check that the current caller id is a valid mobile israel phone number    
                     if(validetor.IsisraelMobile(call.CallerID)){
                    // We update the costume data with the aprove phone number
                    call.SetParam("SendSMSTo",call.CallerID)
                    //Goodbye
                    call.GoToLayer(403) 
                }
                    // not a valid Israel mobile number. "please dialed a new number".
                    else {
                    call.GoToLayer(402)  
                }
 
                break;
                
                // if dialed "2" the client wants to enter a new caller id 
                case "2":  
                    //"please enter a new phone number"
                    console.log('401 2');
                    call.GoToLayer(404)
                    break;
        
                // if the caller did not choose an options or pressed on a wrong digit     
                default:
                     //"you dialed the wrong option" directs afterwards to the main layer - '13'
                    call.GoToLayer(401)
                    break;
            }             
                
        break;

        // layer '404' the caller enters a new mobile phone number
        case '404':
                // we check that the new caller id is a valid mobile israel phone number    
                if(validetor.IsisraelMobile(call.DTMF)){
                    // We update the costume data with the aprove phone number
                    call.SetParam("SendSMSTo",call.DTMF)
                    //Please aprove the mobile number
                    call.Say({sayData: sayDataDTMF2},406,"HE")
                }
                // not a valid Israel mobile number. "please dialed a new number".
                else {
                    call.GoToLayer(405)  
                }
        break;   

         // layer '402' the caller callerId is not an Israeli mobile number, please enter a valid number
         case '402':
                // we check that the new caller id is a valid mobile israel phone number    
                if(validetor.IsisraelMobile(call.DTMF)){
                    // We update the costume data with the aprove phone number
                    call.SetParam("SendSMSTo",call.DTMF)
                    //Please aprove the mobile number
                    call.Say({sayData: sayDataDTMF},406,"HE")
                }
                // not a valid Israel mobile number. "please dialed a new number".
                else {
                    call.GoToLayer(405)  
                }
        break;  

        //'406' - Aproval of the caller id (1 - aprove, 2 - enter new caller id) - we added 406 because using 401 twice update the current caller id and not the DTMF
        case '406':
            console.log('the DTMF is ' + call.DTMF);
            switch (call.DTMF) {
                // if dilaed "1" the caller approve the caller id we identified
                case "1":
                    //Goodbye
                    call.GoToLayer(403) 
                break;
                
                // if dialed "2" the client wants to enter a new caller id 
                case "2":  
                    //"please enter a new phone number"
                    console.log('401 2');
                    call.GoToLayer(404)
                    break;
        
                // if the caller did not choose an options or pressed on a wrong digit     
                default:
                     //"you dialed the wrong option" directs afterwards to the main layer - '13'
                    call.GoToLayer(406)
                    break;
            }             
                
        break;

        // layer '405' the dialed callerId is not an Israeli mobile number, please enter a valid number
        case '405':
            // we check that the new caller id is a valid mobile israel phone number    
            if(validetor.IsisraelMobile(call.DTMF)){
                // We update the costume data with the aprove phone number
                call.SetParam("SendSMSTo",call.DTMF)
                //Please aprove the mobile number
                call.Say({sayData: sayDataDTMF},406,"HE")
            }
            // not a valid Israel mobile number. "please dialed a new number".
            else {
                call.GoToLayer(405)  
            }
        break;  

        // 500s layers are generic SMS
        //'500' - We say the caller ID that we identified
        case '500':            
                //"The caller id is:  "
                call.Say({sayData: sayDataCallerID},501,"EN")            
        break;

        //'501' - Aproval of the caller id (1 - aprove, 2 - enter new caller id)
        case '501':
            console.log('the DTMF is ' + call.DTMF);
            switch (call.DTMF) {
                // if dilaed "1" the caller approve the caller id we identified
                case "1":
                    //we check that the current caller id is a valid mobile israel phone number    
                     if(validetor.IsisraelMobile(call.CallerID)){
                    // We update the costume data with the aprove phone number
                    call.SetParam("SendSMSTo",call.CallerID)
                    //Goodbye
                    call.GoToLayer(503) 
                }
                    // not a valid Israel mobile number. "please dialed a new number".
                    else {
                    call.GoToLayer(502)  
                }
 
                break;
                
                // if dialed "2" the client wants to enter a new caller id 
                case "2":  
                    //"please enter a new phone number"
                    console.log('501 2');
                    call.GoToLayer(504)
                    break;
        
                // if the caller did not choose an options or pressed on a wrong digit     
                default:
                     //"you dialed the wrong option" directs afterwards to the main layer - '13'
                    call.GoToLayer(501)
                    break;
            }             
                
        break;

        // layer '504' the caller enters a new mobile phone number
        case '504':
                // we check that the new caller id is a valid mobile israel phone number    
                if(validetor.IsisraelMobile(call.DTMF)){
                    // We update the costume data with the aprove phone number
                    call.SetParam("SendSMSTo",call.DTMF)
                    //Please aprove the mobile number
                    call.Say({sayData: sayDataDTMF2},506,"HE")
                }
                // not a valid Israel mobile number. "please dialed a new number".
                else {
                    call.GoToLayer(505)  
                }
        break;   

         // layer '502' the caller callerId is not an Israeli mobile number, please enter a valid number
         case '502':
                // we check that the new caller id is a valid mobile israel phone number    
                if(validetor.IsisraelMobile(call.DTMF)){
                    // We update the costume data with the aprove phone number
                    call.SetParam("SendSMSTo",call.DTMF)
                    //Please aprove the mobile number
                    call.Say({sayData: sayDataDTMF},506,"HE")
                }
                // not a valid Israel mobile number. "please dialed a new number".
                else {
                    call.GoToLayer(505)  
                }
        break;  

        //'506' - Aproval of the caller id (1 - aprove, 2 - enter new caller id) - we added 506 because using 501 twice update the current caller id and not the DTMF
        case '506':
            console.log('the DTMF is ' + call.DTMF);
            switch (call.DTMF) {
                // if dilaed "1" the caller approve the caller id we identified
                case "1":
                    //Goodbye
                    call.GoToLayer(503) 
                break;
                
                // if dialed "2" the client wants to enter a new caller id 
                case "2":  
                    //"please enter a new phone number"
                    console.log('501 2');
                    call.GoToLayer(504)
                    break;
        
                // if the caller did not choose an options or pressed on a wrong digit     
                default:
                     //"you dialed the wrong option" directs afterwards to the main layer - '13'
                    call.GoToLayer(506)
                    break;
            }             
                
        break;

        // layer '505' the dialed callerId is not an Israeli mobile number, please enter a valid number
        case '505':
            // we check that the new caller id is a valid mobile israel phone number    
            if(validetor.IsisraelMobile(call.DTMF)){
                // We update the costume data with the aprove phone number
                call.SetParam("SendSMSTo",call.DTMF)
                //Please aprove the mobile number
                call.Say({sayData: sayDataDTMF},506,"HE")
            }
            // not a valid Israel mobile number. "please dialed a new number".
            else {
                call.GoToLayer(505)  
            }
        break; 
    
         // layer '505' TEST
         case '155':
                //"The DTMF  is:  "
                call.Say({sayData: sayDataDTMF},1,"EN");
                console.log("!!! ",sayDataDTMF);
            break; 

    }
}

